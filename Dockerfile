FROM ruby:2.5

ENV LC_ALL=C.UTF-8
ENV JEKYLL_ENV=production


RUN apt update && apt install nodejs -y

COPY . /app/
WORKDIR /app/

RUN bundle install

CMD bundle exec jekyll serve -H 0.0.0.0
