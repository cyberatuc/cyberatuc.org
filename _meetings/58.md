---
title: "Cool GitHub Projects!"
date: 2018-06-27 18:30:00
meeting_number: 58
---
This week, we had planned to look at cool starred GitHub projects, but we ended up saving it for next meeting.

## Weekly Content
* [FIFA on cybersecurity](https://securelist.com/fifa-public-wi-fi-guide/85919/)
  * [additional article](https://www.welivesecurity.com/2018/06/27/world-cup-squads-briefed-cybersecurity-best-practices/)
* [PBot](https://securelist.com/pbot-evolving-adware/86242/)
  * [More on TheHackerNews](https://thehackernews.com/2018/06/pythonbot-pbot-adware.html)
* [WPA3](https://www.welivesecurity.com/2018/06/26/113523/)
  * [Technical info](https://ieeexplore.ieee.org/document/4622764/)
* [Have I Been Pwned built into Firefox/1Password](https://www.darkreading.com/endpoint/have-i-been-pwned-now-built-into-firefox-1password/d/d-id/1332152)
  * [Troy Hunt's blog post](https://www.troyhunt.com/were-baking-have-i-been-pwned-into-firefox-and-1password/)
* Additional reading
  * <https://www.darkreading.com/application-security/ieee-calls-for-strong-encryption/d/d-id/1332159>
  * <https://krebsonsecurity.com/2018/06/supreme-court-police-need-warrant-for-mobile-location-data/>
  * <https://thehackernews.com/2018/06/free-ransomware-decryption-tools.html>
  * <https://thehackernews.com/2018/06/wordpress-hacking.html>
