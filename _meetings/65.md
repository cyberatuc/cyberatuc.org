---
title: "Planning for Fall Semester"
date: 2018-08-22 18:30:00
meeting_number: 65
meeting_slides: false
---
This week was a less formal meeting — we met in ERC 516 and worked on planning and administrative tasks relating to getting ready for the start of the semester.
