---
title: "Introduction to Linux Distributions"
date: 2018-10-16 18:30:00
meeting_number: 73
youtube: https://www.youtube.com/watch?v=PDwAvfsie_w
---
This week, we went over some common Linux distros.

## Weekly Content
* [WhatsApp Video Call Compromise](https://thehackernews.com/2018/10/hack-whatsapp-account-chats.html)
* [LoJax UEFI rootkit by Sednit APT](https://www.welivesecurity.com/2018/09/27/lojax-first-uefi-rootkit-found-wild-courtesy-sednit-group/)
* [Web Polluters: Xiongmai](https://krebsonsecurity.com/2018/10/naming-shaming-web-polluters-xiongmai/)
* Recommended reading
    * <https://thehackernews.com/2018/10/android-linux-kernel-cfi.html>
    * <https://thehackernews.com/2018/10/android-cloud-backup.html>
    * <https://thehackernews.com/2018/10/web-browser-tls-support.html>
    * <https://www.darkreading.com/operations/ibm-builds-soc-on-wheels-to-drive-cybersecurity-training/d/d-id/1333042>
    * <https://krebsonsecurity.com/2018/10/supply-chain-security-101-an-experts-view/>
    * <https://www.welivesecurity.com/2018/10/11/new-telebots-backdoor-linking-industroyer-notpetya/>
    * <https://www.darkreading.com/cloud/millions-of-voter-records-found-for-sale-on-the-dark-web/d/d-id/1333041>
    * <https://www.darkreading.com/endpoint/privacy/dod-travel-system-breach-exposed-data-of-30k-civilian-military-employees/d/d-id/1333036>
    * <https://thehackernews.com/2018/10/dark-web-drugs-kingpin.html>
