---
title: "MITRE Framework (continued)"
date: 2018-10-30 18:30:00
meeting_number: 75
youtube: https://www.youtube.com/watch?v=vndpYtmh3xk
---
This week, we continued learning about MITRE Frameworks.

## Weekly Content
* [Windows Zero-Day](https://thehackernews.com/2018/10/windows-zero-day-exploit.html)
* [DustSquad](https://securelist.com/octopus-infested-seas-of-central-asia/88200/)
* Recommended Reading
    * <https://thehackernews.com/2018/10/android-security-updates.html>
    * <https://krebsonsecurity.com/2018/10/mirai-co-author-gets-6-months-confinement-8-6m-in-fines-for-rutgers-attacks/>
    * <https://krebsonsecurity.com/2018/10/how-do-you-fight-a-12b-fraud-problem-one-scammer-at-a-time/>
    * <https://thehackernews.com/2018/10/ibm-redhat-tech-acquisition.html>
    * <https://thehackernews.com/2018/10/facebook-cambridge-analytica.html>
    * <https://thehackernews.com/2018/10/russia-triton-ics-malware.html>
    * <https://thehackernews.com/2018/10/privilege-escalation-linux.html>
    * <https://thehackernews.com/2018/10/windows-defender-antivirus-sandbox.html>
    * <https://securelist.com/darkpulsar-faq/88233/>
