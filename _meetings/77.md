---
title: "Goat Dissasembly"
date: 2018-11-13 18:30:00
meeting_number: 77
youtube: https://www.youtube.com/watch?v=vndpYtmh3xk
---
This week, we did a Goat dissasembly demo.

## Weekly Content
* [StatCounter Hijacked Leads To Bitcoin Theft](https://thehackernews.com/2018/11/statcounter-cryptocurrency-cyberattack.html)
* [VirtualBox Flaw, Escaping The Sandbox](https://thehackernews.com/2018/11/virtualbox-zero-day-exploit.html)
* [Bleeding Bit](https://thehackernews.com/2018/11/bluetooth-chip-hacking.html)
* Recommended Reading
    * <https://thehackernews.com/2018/11/android-in-app-updates-api.html>
    * <https://krebsonsecurity.com/2018/11/u-s-secret-service-warns-id-thieves-are-abusing-uspss-mail-scanning-service/>
    * <https://www.darkreading.com/vulnerabilities---threats/the-morris-worm-turns-30-/d/d-id/1333225>
    * <https://www.welivesecurity.com/2018/11/05/malware-1980s-brain-virus-morris-worm/>
    * <https://www.welivesecurity.com/2018/11/09/us-air-force-hackable-bug-bounty-program/>
    * <https://krebsonsecurity.com/2018/11/bug-bounty-hunter-ran-isp-doxing-service/>
    * <https://thehackernews.com/2018/11/gaming-server-ddos-attack.html>
    * <https://www.welivesecurity.com/2018/11/08/cyber-insurance-question/>
    * <https://www.welivesecurity.com/2018/11/09/emotet-launches-major-new-spam-campaign/>
    * <https://thehackernews.com/2018/11/portsmash-intel-vulnerability.html>
    * <https://thehackernews.com/2018/11/self-encrypting-ssd-hacking.html>
    * <https://thehackernews.com/2018/11/woocommerce-wordpress-hacking.html>
