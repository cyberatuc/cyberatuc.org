---
title: "Intel Management Engine and Other Coprocessors"
date: 2019-01-16 18:30:00
meeting_number: 81
youtube: https://www.youtube.com/watch?v=X4ONPLOG6J4
---
Welcome back from winter break! We have now changed our meeting time from Tuesday to Wednesday. This week we talked about security issues with Intel Management Engine (IME).

## Weekly Content
* <https://www.washingtonpost.com/news/powerpost/paloma/the-cybersecurity-202/2019/01/08/the-cybersecurity-202-how-one-key-democrat-plans-to-watchdog-offensive-hacking-operations/5c338eba1b326b66fc5a1bc8/?noredirect=on&utm_term=.7ce98e45f404>
* <https://jalopnik.com/if-you-can-hack-into-this-tesla-model-3-its-yours-1831746885>
