---
title: "Cyber@UC CTF-TOOL Demo"
date: 2019-02-27 18:30:00
meeting_number: 86
meeting_slides: false
---
This week, we showed off our new internal ctf-tool which is now available on gitlab. ctf-tool is meant to modularly deploy premade CTF's to a server and to CTFd for our future outreach and events.

More info on ctf-tool on <a href="https://gitlab.com/cyberatuc/ctf-tool">GitLab</a>. ctf-tool is still a work in progress and subject to frequent change.

This meeting was not recorded but feel free to checkout our other meetings on youtube.
