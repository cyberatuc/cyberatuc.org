---
title: "Weekly Blog: Lakota East CTF Outreach"
date: 2018-10-15 19:11:24
author: young2r2
---
This week Hayden, Cory, and I went to Lakota East's Hack Club to introduce the high school students to CTF basics. We taught them HTML, XSS, and communication with web servers. The students were all very new to the subject and were happy to learn about vast world of hacking. We gave them a cross-site-scripting activity and walked them through it. It was a great introduction for them. In the coming weeks we are going to go back and give them an official CTF for them to work on.

## Cyber@UC Wiki now live

This week, we finally got our wiki setup for permanent documentation. It will contain the documentation for the lab and other information that may need to be added. You can find it at <https://wiki.cyberatuc.org>

To get access to confidential docs, register an account then message  Ryan Young on Slack to get permission. The accounts on the wiki will be temporary until we can get _LDAP_ setup with OpenStack.

## Lab Progress

We planed out VLANs for our switches. We have also started looking into _Apache Metron, Ansible, Vagrant._ We are now going to implement the VLANs next week.
