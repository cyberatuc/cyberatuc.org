---
layout: page
title: Members
permalink: /members/
---
Cyber@UC is a student-run cybersecurity organization at the University of Cincinnati. We spread the knowledge and importance of cybersecurity across all ages by engaging in community outreach events or by collaboration with companies and organizations.

Our faculty advisors are [Dr. Will Hawkins](https://researchdirectory.uc.edu/p/hawkinwh) and [Dr. Boyang Wang](https://researchdirectory.uc.edu/p/wang2ba).

## Executives
<div class="row">
  {%- for usr in site.data.team.executives -%}
    <div class="mx-5 mx-sm-0 col-sm-6 col-md-4 col-xl-3">
      {% if forloop.index0 > 3 %}
        {% include user-card.html username=usr break_names=false %}
      {% else %}
        {% include user-card.html username=usr %}
      {% endif %}
    </div>
    {% if forloop.index0 == 3 %}
      <div class="w-100 d-none d-xl-block"></div>
    {% endif %}
  {%- endfor -%}
</div>
<div class="row">
  {%- for usr in site.data.team.members -%}
    <div class="col-6 col-sm-4 col-md-3 col-lg-2">
      {% include user-card.html username=usr small=true %}
    </div>
  {%- endfor -%}
</div>
---
## Alumni
<div class="row">
  {%- for usr in site.data.team.alumni -%}
    <div class="col-6 col-sm-4 col-md-3 col-lg-2">
      {% include user-card.html username=usr small=true %}
    </div>
  {%- endfor -%}
</div>
