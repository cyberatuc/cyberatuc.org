---
layout: page
title: 'BearcatCTF 2025: World Tour'
permalink: /bearcatctf/
---
<!-- 5/27/2024: Need to include information about previous BearcatCTF. Include the highlights PDF. More information coming soon page.-->
Welcome to Cyber@UC's BearcatCTF 2025: World Tour! This is Cyber@UC's second year running the BearcatCTF competition. Last year's competition saw over 400 participants across the globe spread across 246 teams. The competitors solved a combined 905 problems! Top-ranked in-person students brought home $1,700 in prizes. For more information check out the <a href="/files/BearcatCTF/bearcatctf2024_summary.pdf" download="bearcatctf2024_summary">BearcatCTF 2024 Summary Packet!</a>
<br>

This event is welcoming students across Ohio, Northern Kentucky, and Southern Indiana to compete in-person at the 1819 Innovation Hub. Competing at the tri-state level provides the opportunity to work with and against some of the brightest student minds in the area. Students will be able to compete online across the country and around the world. Registration is <b>FREE!</b>

<center><a class="btn btn-primary btn-lg" href="https://bearcatctf.io" target="_blank" title="Bearcat CTF 2025">Register Now!</a></center>
<div class="row">
    <div class="col-md-7 col-lg-7">
        <h2>Event Information</h2>
        <p>
            BearcatCTF 2025 is a 24-hour, continuous event hosted at the University of Cincinnati
            <ul>
                <li>Event Start: Feburary 1st at 10AM</li>
                <li>CTF Start: February 1st at Noon</li>
                <li>CTF End: February 2nd at Noon</li>
                <li>Event End: Feburary 2nd at 1PM</li>
                <li>Location: 1819 Innovation Hub, 2900 Reading Rd, Cincinnati, OH 45206</li>
                <li>Teams of up to 4 people. (Team formation happens closer to the event)</li>
                <li>18+ students only</li>
            </ul>
            <h2>Prizes</h2>
                <p>Prizes will be announced in the future</p>
                <b>*Note: Competitors are eligible for prizes ONLY if they are attending in-person</b>
        </p>
    </div>
    <div class="col-md-5 col-lg-5">
      <a href="{% asset bearcat25_logo_final.png @path %}" target="_blank">
        {% asset bearcat25_logo_final.png
          alt="Bearcat CTF Logo" class="img-fluid"
          %}
      </a>
    </div>
  </div>
<div class="row">
    <div class="col-md-6 col-lg-12">
        <h2>Sponsorship Packages</h2>
            <p>
                An event like this is impossible without sponsors. Below are sponsorship packages for both BearcatCTF and the Cyber@UC student organization. If interested, please email cyberatuc513@gmail.com.
            </p>
            <!--<a href="/files/BearcatCTF/cyber_at_uc_sponsorship_2023.pdf" download="cyberatucsponsorship">Sponsorship Download</a>-->
            <table class="table table-bordered table-condensed">
                <thead>
                <tr>
                    <th>
                        <a href="{% asset bearcatctf-long.png @path %}" target="_blank">
                        {% asset bearcatctf-long.png alt="Bearcat CTF Logo" height=100%}
                        </a>
                    </th>
                    <th><b>Bronze: $500</b></th>
                    <th><b>Silver: $1250</b></th>
                    <th><b>Gold: $2250</b></th>
                    <th><b>Platinum: $5000</b></th>
                </tr>
                </thead>
                <tbody>
                <tr>
                    <td><b>Branding -</b> Feature your logo on event shirts and website</td>
                    <td><b>x</b></td>
                    <td><b>x</b></td>
                    <td><b>x</b></td>
                    <td><b>x</b></td>
                </tr>
                <tr>
                    <td><b>Career Link -</b> Link to your company's career page or email</td>
                    <td><b>x</b></td>
                    <td><b>x</b></td>
                    <td><b>x</b></td>
                    <td><b>x</b></td>
                </tr>
                <tr>
                    <td><b>Career Fair -</b> Dedicated booth space to talk to participants about career opportunities</td>
                    <td></td>
                    <td><b>x</b></td>
                    <td><b>x</b></td>
                    <td><b>x</b></td>
                </tr>
                <tr>
                    <td><b>Keynote Feature -</b> 5 minutes at the beginning of the keynote to talk</td>
                    <td></td>
                    <td><b>x</b></td>
                    <td><b>x</b></td>
                    <td><b>x</b></td>     
                </tr>
                <tr>
                    <td><b>Sponsor Challenge -</b> Bring your own challenge to the CTF!</td>
                    <td></td>
                    <td><b>x</b></td>
                    <td><b>x</b></td>
                    <td><b>x</b></td>
                </tr>
                <tr>
                    <td><b>Host Session or Workshop -</b> Talk with students about educational topic related to your field of work</td>
                    <td></td>
                    <td></td>
                    <td><b>x</b></td>
                    <td><b>x</b></td>
                </tr>
                <tr>
                    <td><b>Keynote -</b> Invitation to talk during keynote speech at event kickoff</td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td><b>x</b></td>
                </tr>
                </tbody>
            </table>
    </div>
</div>
<!-- <div class="row">
    <div class="col-md-12 col-lg-12">
        <h2>CTF Participants</h2>
            <p>
                All CTF Particpants are required to register online at <a href="https://bearcatctf.io" target="_blank" title="Bearcat CTF 2024"> bearcatctf.io</a>
            </p>
        <a class="btn btn-primary btn-lg" href="https://bearcatctf.io" target="_blank" title="Bearcat CTF 2024">Registration</a>
    </div>
</div>
<div class="row">
    <div class="col-md-12 col-lg-12">
        <h2>Volunteers</h2>
            <p>
                If you are a UC student and interested in helping prepare for the CTF or run the event please contact cyberatuc513@gmail.com. You do not have to be a member of Cyber@UC or even interested in cybersecurity to help out. This is a great opportunity for service hours.
            </p>
    </div>
</div> -->
