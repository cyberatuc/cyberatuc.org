---
layout: page
title: Guides
permalink: /guides/
---
This page is for hosting tutorials, guides, and other helpful information.

## Tutorials
* VirtualBox videos
    * [Part 1](https://www.youtube.com/watch?v=miVb-hCkAEA)
    * [Part 2](https://www.youtube.com/watch?v=eTsuo5KJi-M)
* [Linux: Mounting Drives]({{ site.url }}/guides/mounting-drives)
* [App installation scripts for Ubuntu]({{ site.url }}/guides/app-installers)

## Cyber@UC meta topics
* [Contributing to cyberatuc.org]({{ site.url }}/guides/website)
* [Running the Cyber@UC livestream]({{ site.url }}/guides/livestream)
