---
layout: page
title: Join
permalink: /join/
---
All majors, experiences, and graduate level are welcome!
The club is free to join with no commitment required.
We have plenty of opportunties available for students looking to get leadership experience or take on further responsibilities.
Our membership consists of Computer Science (CS), IT, Cybersecurity (IT), and Cybersecurity Engineering majors.

All Cyber@UC members are all on [our discord](https://discord.gg/EWn7tFnVdb).
This is the best way to communicate with everyone.
Come to one general body meeting or committee meeting to get verified on the discord. Ask an executive at the end of the meeting to do so.

Industry and academic speakers wishing to contact us should contact us via email:  <b>cyberatuc513@gmail.com</b>
