---
layout: page
title: Sponsors
#permalink: /sponsor/

#The sponsor page has been archived due to the changes to university policy making it a pain to actually do sponsors
---
Cyber@UC is continually looking to develop relationships with companies to help grow the club and your young talent. Your financial support will allow us to invest in technologies for our general body and for us to host campus events and remote CTF games. These
investments attract the brightest and most motivated students our university has to offer. In return, we can provide many great ways for your organization to connect with our students, and the students of the University of Cincinnati for a full year! If you are interested in our CTF specific sponsorship package, checkout our <a href="/bearcatctf">BearcatCTF</a> page for more information. For any questions please reach out to <b>cyberatuc513@gmail.com</b> or the designated club treasurer (<a href="/members">see Members.</a>)

<table class="table table-bordered table-condensed">
    <thead>
        <tr>
            <th>
               <a href="{% asset logo.jpg @path %}" target="_blank">
                    {% asset logo.jpg alt="Cyber@UC Logo" height=100%}
                </a>
            </th>
                <th><b>Bit: $1750</b></th>
                <th><b>Byte: $2750</b></th>
        </tr>
    </thead>
    <tbody>
        <tr>
            <td><b>CTF Silver Tier</b></td>
            <td><b>x</b></td>
            <td><b>x</b></td>
        </tr>
        <tr>
            <td><b>Branding -</b> Feature your logo on our club shirts and website</td>
            <td><b>x</b></td>
            <td><b>x</b></td>
        </tr>
        <tr>
            <td><b>Resume Book -</b> Access to our active general body's resumes</td>
            <td><b>x</b></td>
            <td><b>x</b></td>
        </tr>
        <tr>
            <td><b>Meeting Takeover -</b> Come talk to our general body about what you and your company does on a day to day basis.</td>
            <td><b>x</b></td>
            <td><b>x</b></td>
        </tr>
        <tr>
            <td><b>Capstone Connections -</b> Connect with senior students working on their final project for their undergrad degree.</td>
            <td><b>x</b></td>
            <td><b>x</b></td>
        </tr>
        <tr>
            <td><b>CTF Gold Tier</b></td>
            <td></td>
            <td><b>x</b></td>
        </tr>
        <tr>
            <td><b>Industry Dinner -</b> Have a catered dinner (or other event) with our general body and members of campus</td>
            <td></td>
            <td><b>x</b></td>
        </tr>
    </tbody>
</table>
